// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter_opengl_view/flutter_opengl_view.dart';
import 'dart:ffi';  // For FFI
import 'dart:io';   // For Platform.isX

import 'package:flutter/services.dart';
import 'package:flutter_opengl_example/flutter_opengl_example.dart';

final DynamicLibrary openGlLib =
  Platform.isAndroid
    ? DynamicLibrary.open("libflutter_opengl_example.so")
    : DynamicLibrary.process();


final int Function() openGlPlugin =
  openGlLib
    .lookup<NativeFunction<Int64 Function()>>("getFlutterOpenGL")
    .asFunction();

    void main() {
      runApp(const MyApp());
    }

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  late OpenGlViewController openGlViewController;

  @override
  void initState() {
    super.initState();
    // initPlatformState();
  }

  @override
  Widget build(BuildContext context) {

  FlutterOpenglView openGlView = FlutterOpenglView(
      onViewCreated: onViewCreated,
    );


    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Container(
          child: openGlView,
          height: 400.0,
        )
      ),
    );
  }

  void onViewCreated(openGlViewController) {

    this.openGlViewController = openGlViewController;
    //this.openGlViewController.initGl();
    int ref = 0;
    ref = openGlPlugin();
    //this.openGlViewController.initGl();
    this.openGlViewController.initGLNative(ref);
  }
}
