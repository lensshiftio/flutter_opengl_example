// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <FlutterOpenGLFFI.h>
#include <FlutterOpenGLExample.h>

//#include <stdio.h>
//#include <inttypes.h>

//#include <stdint.h>

DART_EXPORT int64_t getFlutterOpenGL()
{
    FlutterOpenGLExample* openGl = new FlutterOpenGLExample();
    int64_t openGlRef = reinterpret_cast<int64_t>(openGl);

    return openGlRef;
}
