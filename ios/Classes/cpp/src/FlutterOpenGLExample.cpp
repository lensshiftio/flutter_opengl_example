// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <FlutterOpenGLExample.h>

#ifdef __ANDROID__
#if __ANDROID_API__ >= 24
#include <GLES3/gl32.h>
#elif __ANDROID_API__ >= 21
#include <GLES3/gl31.h>
#else
#include <GLES3/gl3.h>
#endif
#endif

#ifdef __APPLE__

#include <OpenGLES/ES2/gl.h>  // TODO OGLES 3 ???

#endif

FlutterOpenGLExample::FlutterOpenGLExample()
{

}

FlutterOpenGLExample::~FlutterOpenGLExample()
{

}

void FlutterOpenGLExample::init(int width, int height)
{
    // TODO
    glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

}

void FlutterOpenGLExample::onDrawFrame()
{
    // TODO
    glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}

void FlutterOpenGLExample::stopDrawFrame()
{
    // TODO
}

bool FlutterOpenGLExample::isReady()
{

  return true;
}
