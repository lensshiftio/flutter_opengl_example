// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FLUTTER_PLUGIN_FLUTTEROPENGL_EXAMPLE_H
#define FLUTTER_PLUGIN_FLUTTEROPENGL_EXAMPLE_H

#include <FlutterOpenGL.h>

class FlutterOpenGLExample : public FlutterOpenGL {

public:

    /**
     * @brief Construct a new FlutterOpenGL object
     *
     */
    FlutterOpenGLExample();

    /**
     * @brief Destroy the FlutterOpenGL object
     *
     */
    ~FlutterOpenGLExample();

    void init(int width, int height);

    void onDrawFrame();

    void stopDrawFrame();

    bool isReady();
};

#endif // FLUTTER_PLUGIN_FLUTTEROPENGL_EXAMPLE_H
