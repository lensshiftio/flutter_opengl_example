// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FLUTTER_PLUGIN_FLUTTEROPENGL_FFI_H
#define FLUTTER_PLUGIN_FLUTTEROPENGL_FFI_H

//#ifdef __APPLE__
//
//#else
//    #include <cstdint>
//#endif

#include <cstdint>

#if __cplusplus

#if defined(_WIN32)
#define DART_EXPORT extern "C" __declspec(dllexport)
#else
#define DART_EXPORT                                                            \
  extern "C" __attribute__((visibility("default"))) __attribute((used))
#endif

DART_EXPORT int64_t getFlutterOpenGL();

#endif

#endif // FLUTTER_PLUGIN_FLUTTEROPENGL_FFI_H
