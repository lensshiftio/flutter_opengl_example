// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FLUTTER_PLUGIN_FLUTTEROPENGL_H
#define FLUTTER_PLUGIN_FLUTTEROPENGL_H

class FlutterOpenGL {

public:

    /**
     * @brief Construct a new FlutterOpenGL object
     *
     */
    //FlutterOpenGL();

    /**
     * @brief Destroy the FlutterOpenGL object
     *
     */
    //virtual ~FlutterOpenGL();

    virtual void init(int width, int height) = 0;

    virtual void stopDrawFrame() = 0;

    virtual void onDrawFrame() = 0;

    virtual bool isReady() = 0;
};

#endif // FLUTTER_PLUGIN_FLUTTEROPENGL_H
