# flutter_opengl_example

This plugin shows, how to integrate the [flutter_opengl_view](https://gitlab.com/lensshift/flutter_opengl_view) plugin.
<br>
Please refer to the *flutter_opengl_view* plugin for more information.

# Example
You can have a look at `flutter_opengl_example/ios/Classes/cpp/src/FlutterOpenGLExample.cpp` <br>
<br>
To check if everything is set up correctly, simply change the *clear color*:
``` C++
glClearColor(1.0f, 0.0f, 1.0f, 0.0f);   // change color to purple 
```
