
import 'flutter_opengl_example_platform_interface.dart';

class FlutterOpenglExample {
  Future<String?> getPlatformVersion() {
    return FlutterOpenglExamplePlatform.instance.getPlatformVersion();
  }
}
