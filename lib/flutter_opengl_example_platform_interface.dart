import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_opengl_example_method_channel.dart';

abstract class FlutterOpenglExamplePlatform extends PlatformInterface {
  /// Constructs a FlutterOpenglExamplePlatform.
  FlutterOpenglExamplePlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterOpenglExamplePlatform _instance = MethodChannelFlutterOpenglExample();

  /// The default instance of [FlutterOpenglExamplePlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterOpenglExample].
  static FlutterOpenglExamplePlatform get instance => _instance;
  
  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterOpenglExamplePlatform] when
  /// they register themselves.
  static set instance(FlutterOpenglExamplePlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
