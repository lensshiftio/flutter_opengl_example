import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_opengl_example_platform_interface.dart';

/// An implementation of [FlutterOpenglExamplePlatform] that uses method channels.
class MethodChannelFlutterOpenglExample extends FlutterOpenglExamplePlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_opengl_example');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
