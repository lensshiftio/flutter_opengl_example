import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_opengl_example/flutter_opengl_example.dart';
import 'package:flutter_opengl_example/flutter_opengl_example_platform_interface.dart';
import 'package:flutter_opengl_example/flutter_opengl_example_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFlutterOpenglExamplePlatform 
    with MockPlatformInterfaceMixin
    implements FlutterOpenglExamplePlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final FlutterOpenglExamplePlatform initialPlatform = FlutterOpenglExamplePlatform.instance;

  test('$MethodChannelFlutterOpenglExample is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFlutterOpenglExample>());
  });

  test('getPlatformVersion', () async {
    FlutterOpenglExample flutterOpenglExamplePlugin = FlutterOpenglExample();
    MockFlutterOpenglExamplePlatform fakePlatform = MockFlutterOpenglExamplePlatform();
    FlutterOpenglExamplePlatform.instance = fakePlatform;
  
    expect(await flutterOpenglExamplePlugin.getPlatformVersion(), '42');
  });
}
